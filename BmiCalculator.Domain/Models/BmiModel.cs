﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BmiCalculator.Domain.Models {
    //public bijzetten, standaard private
    //geen constructor in mvvm 99% vn de keer
    public class BmiModel {
        private double _gewicht;
        private double _lengte;

        public double Gewicht {
            get {return _gewicht;}
            set {_gewicht = value;}
        }

        public double Lengte {
            get { return _lengte; }
            set { _lengte = value; }
        }
        private double _bmiWaarde;

        public double BmiWaarde {
            get { return _bmiWaarde; }
            //bereikbaar voor iedereen in de zelfde assembly (BmiCalculator.Domain)
            internal set { _bmiWaarde = value; }
        }
        //overriding the standard methods (toString)
        /*
         * public override string ToString(){
         * return base.ToString();
         * }
         */
    }
}
