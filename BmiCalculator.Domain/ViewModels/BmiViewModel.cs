﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BmiCalculator.Domain.Models;

namespace BmiCalculator.Domain.ViewModels {
    /* ViewModel bevat minimum altijd de volgende zaken:
     * -Constructor
     * -Property v/h type Business Model (hier prop BmiModel)
     * -Private Methods (indien ndg)
     * -Public Methods (welke zich in java Model vinden)
     */
   public class BmiViewModel {
        //private field of variabelen en constanten

        //Properties
        private BmiModel _bmi;

        public  BmiModel Bmi {
            get {
                // if(bmi==null) _bmi = new BmiModel();
                return _bmi ?? (_bmi = new BmiModel()); }
            set { _bmi = value; }
        }

        public BmiViewModel() {
            Bmi.Lengte = 1.90;
            Bmi.Gewicht = 80;
            CalculateBmi();
        }

        private void CalculateBmi() {
            Bmi.BmiWaarde = Math.Round(Bmi.Gewicht / (Bmi.Lengte * Bmi.Lengte), 2);
        }
    }
}
